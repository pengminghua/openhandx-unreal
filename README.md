# openhandx unreal

1. 前言
- OpenHandx-Unreal是一个可扩展的轻量级web框架。Unreal以Ajax技术为核心，开创了SS（Script-Service）脚本服务框架开发模式，并完全兼容MVC（Model-View-Controller）框架。SS框架可以轻易完成页面的 Script对象与Pojo对象的创建、操作、转化、传输，实现了web层与Service层的完全分离和解耦，使Service层的领域驱动设计能彻底地贯彻。Unreal还提供了统一的Web Service功能，为企业打造了统一的服务总线奠定基础。Unreal封装严密，使用时不需要学习太多的抽象类、接口等概念，因此容易上手，开发效率更高。
2. 主要特性
- 新创的SS（Script-Service）框架
- 完全兼容MVC（Model-View-Controller）框架
- 原生支持Ajax技术
- 简化的xml配置或零配置
- Web Service功能
- 文件上传支持
- Spring支持

3. SS（Script-Service）脚本服务框架
- 传统的MVC（Model-View-Controller）框架是对页面Form提交请求->应答刷新页面这种情况设计的，因此支持非常好，并得到了广泛的应用。
但是随着web2.0的普及Ajax技术得到应用的场景越来越多，而Ajax应答是具体数据，因此MVC框架并不适合Ajax。SS框架采用JavaScript提交请求，服务端采用Service方式提供服务，最后将应答的结果返回到客户端，JavaScript再将Json数据解析成JavaScript对象。
- **Unreal SS框架体系结构**

![输入图片说明](https://gitee.com/uploads/images/2018/0417/151855_e92716a5_1878748.png "ss.png")
- 1、客户端的JavaScript（调用asynService或synService方法）发起一个请求
- 2、UnrealSS Servlet和SsDispatch查找请求和确定适当的Service
- 3、SsDispatch根据请求的方法和参数名确定要调用的Service的方法，再将参数转化成方法参数需要的数据对象
- 4、Service方法执行该时调用相应的业务逻辑，并获取返回结果（Pojo对象）
- 5、将结果（Pojo对象）转化成Json字符串返回到客户端
- 6、客户端的将Json字符串转化成JavaScript对象
- 客户端页面调用后台的Service方法就象调用本地的JavaScript方法一样简单，并且Service方法返回的Pojo对象也被转化成了同样的JavaScript对象。因此客户端页面可以直接获取Pojo对象的数据。
- SS框架参数获取不需要Form或Action属性作为输入属性，而是通过Service方法的参数获取。SS框架接受的参数也不再是页面的Form，而是JavaScript数据对象
例如有一个JavaScript对象：
```
{
    msg:"hello",
    count:"10",
    amount:"20.1",
    time:"2012-1-1 1:2:3.4"
}
```
被调用Service的方法：
```
public Result input(String msg , int count , float amount , Timestamp time)
```
假设Result有以下属性
```
	public String getText() {
		return text;
	}
	public void setText (String text) {
		this.text = text;
	}
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney (BigDecimal money) {
		this.money = money;
	}
```
最终返回到浏览器的JavaScript对象为
```
{
    text:"hello world",
    money:"100.00"
}
```
input方法被执行时注入的参数msg为"hello",count为10，amount为20.1，time为2012-1-1 1:2:3.4。返回也是将Java对象转化成JavaScript对象。这些都是有SS框架完成的。
SS框架就是客户端脚本直接调用后台服务的框架，对应规则是依赖后台服务的类名、方法名、参数名。
- **SS框架的优点：**
- 1、做web开发者只要关注页面和JavaScript代码，而不必写ActionForm及其他Java层的代码，去除了页面与后台代码的偶合度，使得页面代码复用得到了提高，也利于本地调试。这些都大大节提高了开发的质量和效率，使web开发者有更多的时间关注页面的友好性
- 2、做后台开发者只要按照领域驱动的思想完成Domain、DAO、Service等，开发的是纯粹的业务代码，完全不用关注页面的实现，使得领域驱动的设计思想更加自由，同时代码的复用和测试变得更加容易
4. MVC（Model-View-Controller）框架支持
- 传统的MVC（Model-View-Controller）框架应用广泛，Unreal为了兼容这种应用模式，满足特定开发者的需求，Unreal的MVC框架提供了两种方式：Action方式、Ajax方式。
- **Action方式**
 
![输入图片说明](https://gitee.com/uploads/images/2018/0417/152922_9d194e5c_1878748.png "mvc.png")
- 1.客户端Web浏览器的发起一个请求
- 2.UnrealAction Servlet和ActionDispatch查找请求和确定适当的Action
- 3.ActionDispatch根据请求的方法和参数名确定要调用的Action的方法， 再将页面数据转化Form对象
- 4.Action方法执行该时调用相应的业务逻辑 
- 5.Action执行的结果（分发的页面路径），定向到相应的页面
- 6.最终页面返回客户端Web浏览器

- **Ajax方式**
 
![输入图片说明](https://gitee.com/uploads/images/2018/0417/152929_e61a1abd_1878748.png "ajax.png")
- Unreal Ajax方式与Action方式不同在于Ajax的Action返回的是数据对象（Pojo）这个对象最终以Json字符串返回到客户端，由客户端转化成JavaScript对象。
Unreal MVC的优点：
- 1、Action类不用实现任何接口，可以使一个普通的java类
- 2、Action类是保证线程安全的，每一次请求产生一个实例（使用Spring容器原型模式管理Action的时候除外）
- 3、Action类不依赖Servlet API的HttpServletRequest和HttpServletResponse，便于通过初始化、设置属性、调用方法来测试，“依赖注入”支持也使测试更容易
- 4、Unreal MVC可以使用Action+Form方式也可以是ActionForm方式传递参数
- 5、Unreal MVC提供了Ajax方式的原生支持
5. 文件上传支持
- UnrealContext是上下文管理类，保存了当前请求的HttpServletRequest、HttpServletResponse、ServletContext。除此之外还提供了文件上传的支持，UploadSuppport的getFileItems()方法返回当前请求所有上传文件。
6. Web Service功能
- Web Service功能分为OpenService和OpenClient， OpenClient负责与OpenService通讯、调用、数据转换，OpenService负责服务的提供。
- **体系结构如下：**
- 1.应用程序使用OpenClient调用远程服务器的OpenService
- 2.OpenClient将参数转化成为xml并向OpenService服务器发起请求
- 3.远程服务器OpenService查找请求和确定适当的Service
- 4.远程服务器OpenService根据请求的方法和参数名确定要调用的Service的方法， 再将参数转化成方法参数需要的数据对象
- 5.Service方法执行该时调用相应的业务逻辑，并获取返回结果（Pojo对象）
- 6.远程服务器OpenService将结果（Pojo对象）转化成xml返回到客户端
- 7.应用程序的OpenClient将xml转化 Pojo对象
- **OpenService的特性：**
- 1.Unreal的OpenService是一个通用的WebService工具，包括Service和Client，数据转化xml和传输不需要开发者的关注
- 2.OpenService提供了Java原生的数据类型、BigInteger、BigDecimal、Date、Time、Timestamp、Pojo对象、OpenCommon数据模型（RecordVO、RecordSetVO、MulripleVO）、可串行化对象的传输
- 3.支持Https传输加密和Gzip压缩
7. Spring支持
- Unreal SS和MVC框架以及OpenService的都提供了对Spring的支持。Action、ActionForm、Service层都可以使用Spring Ioc容器管理。
